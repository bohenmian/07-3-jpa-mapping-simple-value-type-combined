package com.twuc.webApp.domain.composite.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class UserProfile {

    @Id
    @GeneratedValue
    private Long id;

    @Column(nullable = false, length = 128)
    private String addressCity;

    @Column(nullable = false, length = 128)
    private String addressStreet;

    public UserProfile(String addressCity, String addressStreet) {
        this.addressCity = addressCity;
        this.addressStreet = addressStreet;
    }

    public UserProfile() {
    }

    public Long getId() {
        return id;
    }

    public String getAddressCity() {
        return addressCity;
    }

    public String getAddressStreet() {
        return addressStreet;
    }
}
