package com.twuc.webApp.domain.composite.repository;

import com.twuc.webApp.domain.composite.model.CompanyProfile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyProfileRepository extends JpaRepository<CompanyProfile, Long> {

}
